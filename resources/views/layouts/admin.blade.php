<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">
    <title>NSTwin Management System</title>
    <!-- chartist CSS -->
    <link href="{{asset('assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('assets/dist/css/style.css')}}" rel="stylesheet">

    @stack('inputmaskcss')
    @stack('datatablescss')
    @stack('customChartCSS')
    @stack('customCSS')
    <!-- This page CSS -->
    <link href="{{asset('assets/dist/css/pages/dashboard1.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="main-wrapper" id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Dexandra Stokist</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <header class="topbar">
            <!-- ============================================================== -->
            <!-- Navbar scss in header.scss -->
            <!-- ============================================================== -->
            <nav>
                <div class="nav-wrapper">
                    <!-- ============================================================== -->
                    <!-- Logo you can find that scss in header.scss -->
                    <!-- ============================================================== -->
                    <a href="/home" class="brand-logo">
                        <span class="icon">
                            <img class="light-logo" width="35px" height="35px" src="{{asset('assets/images/logo-dexandra.png')}}">
                            <img class="dark-logo" width="35px" height="35px" src="{{asset('assets/images/logo-dexandra.png')}}">
                        </span>
                        <span class="text">
                            <img class="light-logo" width="140px" height="23px" src="{{asset('assets/images/logo-text.png')}}">
                            <img class="dark-logo" width="140px" height="23px" src="{{asset('assets/images/logo-text.png')}}">
                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo you can find that scss in header.scss -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Left topbar icon scss in header.scss -->
                    <!-- ============================================================== -->
                    <ul class="left">
                        <li class="hide-on-med-and-down">
                            <a href="javascript: void(0);" class="nav-toggle">
                                <span class="bars bar1"></span>
                                <span class="bars bar2"></span>
                                <span class="bars bar3"></span>
                            </a>
                        </li>
                        <li class="hide-on-large-only">
                            <a href="javascript: void(0);" class="sidebar-toggle">
                                <span class="bars bar1"></span>
                                <span class="bars bar2"></span>
                                <span class="bars bar3"></span>
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Left topbar icon scss in header.scss -->
                    <!-- ============================================================== -->
                    
                </div>
            </nav>
            <!-- ============================================================== -->
            <!-- Navbar scss in header.scss -->
            <!-- ============================================================== -->
        </header>
        <!-- ============================================================== -->
        <!-- Sidebar scss in sidebar.scss -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <ul id="slide-out" class="sidenav">
                
                <li>
                    <ul class="collapsible">

                        <li class="small-cap"><span class="hide-menu">Main Menu</span></li>
                        <li>
                            <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">account_box</i><span class="hide-menu"> Agent</span></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="/agent"><i class="material-icons">group</i><span class="hide-menu">All Agent</span></a></li>
                                    <li><a href="/agent-add"><i class="material-icons">person_add</i><span class="hide-menu">Add Agent</span></a></li>
                                    <li><a href="/agent-suspend"><i class="material-icons">not_interested</i><span class="hide-menu">Suspended Agent</span></a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">account_box</i><span class="hide-menu"> Dropship</span></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="/dropship"><i class="material-icons">group</i><span class="hide-menu">All Dropship</span></a></li>
                                    <li><a href="/dropship-add"><i class="material-icons">person_add</i><span class="hide-menu">Add Dropship</span></a></li>
                                    <li><a href="/dropship-suspend"><i class="material-icons">not_interested</i><span class="hide-menu">Suspended Dropship</span></a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">attach_money</i><span class="hide-menu"> Sales</span></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="/sales"><i class="material-icons">trending_up</i><span class="hide-menu">All Sales</span></a></li>
                                    <li><a href="/sales-new"><i class="material-icons">note_add</i><span class="hide-menu">New Sales</span></a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="/setting" class="collapsible-header"><i class="material-icons">settings</i><span class="hide-menu"> Settings</span></a>
                        </li>

                        {{-- <li>
                            <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dashboard</i><span class="hide-menu">Stock</span></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="/stock"><i class="material-icons">adjust</i><span class="hide-menu">All Stock</span></a></li>
                                    <li><a href="/stock-add"><i class="material-icons">adjust</i><span class="hide-menu">Add Stock Quantity</span></a></li>
                                </ul>
                            </div>
                        </li> --}}
                     
                        <li>
                            <a href="/logout" class="collapsible-header"><i class="material-icons">directions</i><span class="hide-menu"> Log Out </span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- ============================================================== -->
        <!-- Sidebar scss in sidebar.scss -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        @yield('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/materialize.min.js')}}"></script>
    <script src="{{asset('assets/libs/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Apps -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/dist/js/app.js')}}"></script>
    <script src="{{asset('assets/dist/js/app.init.mini-sidebar.js')}}"></script>
    <script src="{{asset('assets/dist/js/app-style-switcher.js')}}"></script>
    <!-- ============================================================== -->
    <!-- Custom js -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/dist/js/custom.min.js')}}"></script>
    @stack('inputmask')
    @stack('datatables')
    @stack('customChartJS')
    @stack('customJS')
</body>

</html>