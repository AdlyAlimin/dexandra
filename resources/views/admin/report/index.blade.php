@extends('layouts.admin')

@push('inputmaskcss')
  <link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    @if ($message = Session::get('success'))
                        <hr>
                        <div class="card-panel white-text green lighten-2">
                            {{-- {{ dd($errors->getBags()) }} --}}
                            {{ $message }}
                        </div>
                    @endif
                    <hr>
                    <div class="col m6">
                        <div class="card">
                            <div class="card-content">
                                <div class="d-flex align-items-center">
                                    <div><i class="material-icons display-6 cyan-text">do_not_disturb</i></div>
                                    <div class="m-l-10">
                                        <h5 class="m-b-0">Suspend Period</h5>
                                        <span class="text-muted">
                                          <a class="waves-effect waves-light btn-small pulse" href="/setting-period/{{ $suspendPeriod->id }}" ><i class="material-icons left">create</i>Change Period</a>
                                        </span>
                                    </div>
                                    <div class="ml-auto">
                                    <h3 class="m-b-0">{{ $suspendPeriod->suspend_period }} Days</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('inputmask')
  <script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
  <script src="{{asset('assets/extra-libs/formatter/jquery.formatter.min.js')}}"></script>

  <script type="text/javascript">
  
//   $('#ic_number').formatter({
//         'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
//         'persistent': true
//     });

  </script>  
@endpush