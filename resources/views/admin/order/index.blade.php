@extends('layouts.admin')

@push('datatablescss')
    <link href="{{asset('assets/dist/css/pages/data-table.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/libs/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    {{-- <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <h5 class="card-title">Sales List</h5>
                                @if ($message = Session::get('success'))
                                    <hr>
                                    <div class="card-panel white-text green lighten-2">
                                        {{ dd($errors->getBags()) }}
                                        {{ $message }}
                                    </div>
                                    <hr>
                                @endif
                                <hr>
                                <table id="file_export" class="table table-striped display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>ID Agent / Dropship</th>
                                            <th>Sales</th>
                                            <th>Sales Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($orders as $order)
                                            <tr>
                                                <td>{{ $order -> id }}</td>
                                                <td>{{ $order -> cust_id }}</td>
                                                <td>{{ $order -> sales }}</td>
                                                <td>{{ $order -> sale_date }}</td>
                                                <td>
                                                    <button onclick="window.location.href='/sales-detail/{{ $order -> id }}/{{ $order -> cust_id }}'" class="waves-effect waves-light btn blue">Detail</button>
                                                    <button onclick="window.location.href='/sales-delete/{{ $order -> id }}'" class="waves-effect waves-light btn red">Delete</button>
                                                </td>
                                            </tr>                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}

                    <div class="col s12">
                        <ul id="tabs-swipe-demo" class="tabs">
                            @if (empty($reportDatas))
                                <li class="tab col s3"><a class="Active" href="#agent">Agent</a></li>
                                <li class="tab col s3"><a href="#dropship">Dropship</a></li>
                                <li class="tab col s3"><a href="#rangeDate">Range Date</a></li>    
                            @else
                                <li class="tab col s3"><a href="#agent">Agent</a></li>
                                <li class="tab col s3"><a href="#dropship">Dropship</a></li>
                                <li class="tab col s3"><a class="Active" href="#rangeDate">Range Date</a></li>
                            @endif
                        </ul>
                    </div>
                    <div id="agent" class="col s12" style="height:auto;">
                        <div class="card">
                            <div class="card-content">
                                <table id="file_export" class="table table-striped display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Total Order</th>
                                            <th>Sale Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($agentOrders as $agent)
                                            <tr>
                                                <td>{{ $agent -> cust_id }}</td>
                                                <td>{{ $agent -> sales }}</td>
                                                <td>{{ $agent -> sale_date }}</td>
                                                <td>
                                                    <button onclick="window.location.href='/sales-detail/{{ $agent -> id }}/{{ $agent -> cust_id }}'" class="waves-effect waves-light btn blue">Detail</button>
                                                    <button onclick="window.location.href='/sales-delete/{{ $agent -> id }}'" class="waves-effect waves-light btn red">Delete</button>
                                                </td>
                                            </tr>                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="dropship" class="col s12" style="height:auto;">
                        <div class="card">
                            <div class="card-content">
                            <table id="file_export2" class="table table-striped display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID Number</th>
                                        <th>Total Order</th>
                                        <th>Sale Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dropshipOrders as $dropship)
                                        <tr>
                                            <td>{{ $dropship -> cust_id }}</td>
                                            <td>{{ $dropship -> sales }}</td>
                                            <td>{{ $dropship -> sale_date }}</td>
                                            <td>
                                                <button onclick="window.location.href='/sales-detail/{{ $dropship -> id }}/{{ $dropship -> cust_id }}'" class="waves-effect waves-light btn blue">Detail</button>
                                                <button onclick="window.location.href='/sales-delete/{{ $dropship -> id }}'" class="waves-effect waves-light btn red">Delete</button>
                                            </td>
                                        </tr>                                            
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                    <div id="rangeDate" class="col s12" style="height:auto;">
                        <div class="card">
                            <div class="card-content">

                                <h5 class="card-title">Select date between to show detail</h5>
                                <form action="/report-sale" method="POST" class="row">
                                    @csrf
                                    <div class="input-field col s12 l4">
                                        <i class="material-icons prefix">insert_invitation</i>
                                        <input type="text" name="firstDate" placeholder="2017-06-04" id="mdate">
                                        <label for="icon_prefix2">Start Date</label>
                                    </div>
                                    <div class="input-field col s12 l4">
                                        <i class="material-icons prefix">insert_invitation</i>
                                        <input type="text" name="secondDate" placeholder="2017-06-04" id="mdate2">
                                        <label for="icon_email">End Date</label>
                                    </div>
                                    <div class="input-field col s12 l4">
                                        <button class="btn cyan waves-effect waves-light" type="submit" name="action">View</button>
                                    </div>
                                </form>

                                @if (empty($reportDatas))
                                    
                                @else

                                    <table id="file_export3" class="table table-striped display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>ID Number</th>
                                                <th>Total Order</th>
                                                <th>Sale Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($reportDatas as $reportData)
                                                <tr>
                                                    <td>{{ $reportData -> cust_id }}</td>
                                                    <td>{{ $reportData -> sales }}</td>
                                                    <td>{{ $reportData -> sale_date }}</td>
                                                    <td>
                                                        <button onclick="window.location.href='/sales-detail/{{ $reportData -> id }}/{{ $reportData -> cust_id }}'" class="waves-effect waves-light btn blue">Detail</button>
                                                        <button onclick="window.location.href='/sales-delete/{{ $reportData -> id }}'" class="waves-effect waves-light btn red">Delete</button>
                                                    </td>
                                                </tr>                                            
                                            @endforeach
                                        </tbody>
                                    </table>

                                @endif

                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('datatables')
    <script src="{{asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#file_export').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>
    <script>
        $('#file_export2').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>
    <script>
        $('#file_export3').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>

    <script src="{{asset('assets/libs/moment/moment.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker-custom.js')}}"></script>
    <script>
        // MAterial Date picker    
        $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
        $('#mdate2').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    </script>
@endpush