@extends('layouts.admin')

@push('datatablescss')
    <link href="{{asset('assets/dist/css/pages/data-table.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
        <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col s12">
                        <ul id="tabs-swipe-demo" class="tabs">
                            <li class="tab col s3"><a class="Active" href="#agent">Agent</a></li>
                            <li class="tab col s3"><a href="#dropship">Dropship</a></li>
                        </ul>
                    </div>
                    <div id="agent" class="col s12" style="height:auto;">
                        <div class="card">
                            <div class="card-content">
                                <table id="file_export" class="table table-striped display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Name</th>
                                            <th>Phone Number</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($agents as $agent)
                                            <tr>
                                                <td>{{ $agent -> agent_id }}</td>
                                                <td>{{ $agent -> fullname }}</td>
                                                <td>{{ $agent -> phone_number }}</td>
                                                <td>
                                                    <button onclick="window.location.href='/sales-agentdetail/{{ $agent -> id }}'" class="waves-effect waves-light btn blue">New Sales</button>
                                                </td>
                                            </tr>                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="dropship" class="col s12" style="height:auto;">
                        <div class="card">
                            <div class="card-content">
                            <table id="file_export2" class="table table-striped display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID Number</th>
                                        <th>Name</th>
                                        <th>Phone Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dropships as $dropship)
                                        <tr>
                                            <td>{{ $dropship -> dropship_id }}</td>
                                            <td>{{ $dropship -> fullname }}</td>
                                            <td>{{ $dropship -> phone_number }}</td>
                                            <td>
                                                <button onclick="window.location.href='/sales-dropshipdetail/{{ $dropship -> id }}'" class="waves-effect waves-light btn red">New Sales</button>
                                            </td>
                                        </tr>                                            
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('inputmask')
  <script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
  <script src="{{asset('assets/extra-libs/formatter/jquery.formatter.min.js')}}"></script>

  <script type="text/javascript">
  
//   $('#ic_number').formatter({
//         'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
//         'persistent': true
//     });

  </script>  
@endpush
@push('datatables')
    <script src="{{asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#file_export').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>
    <script>
        $('#file_export2').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>
@endpush