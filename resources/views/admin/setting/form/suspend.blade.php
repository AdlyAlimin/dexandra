@extends('layouts.admin')

@push('inputmaskcss')
  <link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col m6">
                        <div class="card">
                            <div class="card-content">
                                
                              <form action="/setting-period-save/{{ $setting->id }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="row">
                                    <div class="input-field col s6 m6 l6">
                                        <input id="suspend_period" step='1' min="1" type="number" name="suspend_period" value="{{ $setting->suspend_period }}" required>
                                        <label for="suspend_period">Suspend Period in Days</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button class="btn red waves-effect waves-light right" onclick="window.location.href='/setting'">Cancel</button>
                                        <button class="btn green waves-effect waves-light right" type="submit" name="action">Save</button>
                                    </div>
                                </div>
                              </form>  
                            
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('inputmask')
  <script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
  <script src="{{asset('assets/extra-libs/formatter/jquery.formatter.min.js')}}"></script>

  <script type="text/javascript">
  
//   $('#ic_number').formatter({
//         'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
//         'persistent': true
//     });

  </script>  
@endpush