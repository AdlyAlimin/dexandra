@extends('layouts.admin')

@push('inputmaskcss')
  <link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <h5 class="card-title">New Dropship Registration</h5>
                                @if (count($errors) > 0)
                                    <hr>
                                    <div class="card-panel white-text red lighten-2">
                                        {{-- {{ dd($errors->getBags()) }} --}}
                                        {{ $errors->getBag('default')->first() }}
                                    </div>
                                    <hr>
                                @endif
                                <form action="/dropship-save" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="input-field col s12 m2 l2">
                                            <input id="id" type="text" name="dropship_id" required>
                                            <label for="id">ID</label>
                                        </div>
                                        <div class="input-field col s12 m5 l5">
                                            <input id="fullname" type="text" name="fullname" required>
                                            <label for="fullname">Full Name</label>
                                        </div>
                                        <div class="input-field col s12 m5 l5">
                                            <input id="ic_number" type="text" name="ic_number" required>
                                            <label for="ic_number">Identification Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6 l6">
                                            <input id="email" type="email" name="email" required>
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="input-field col s12 m6 l6">
                                            <input id="phone_number" type="text" name="phone_number" required>
                                            <label for="phone_number">Phone Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea id="address" class="materialize-textarea" length="120" name="address" required></textarea>
                                            <label for="address">Address</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn red waves-effect waves-light right" onclick="window.location.href='/dropship'">Cancel</button>
                                            <button class="btn green waves-effect waves-light right" type="submit" name="action">Save Dropship</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('inputmask')
  <script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
  <script src="{{asset('assets/extra-libs/formatter/jquery.formatter.min.js')}}"></script>

  <script type="text/javascript">
  
//   $('#ic_number').formatter({
//         'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
//         'persistent': true
//     });

  </script>  
@endpush