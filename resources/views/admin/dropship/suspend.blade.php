@extends('layouts.admin')

@push('datatablescss')
    <link href="{{asset('assets/dist/css/pages/data-table.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <h5 class="card-title">Dropship List</h5>
                                @if ($message = Session::get('success'))
                                    <hr>
                                    <div class="card-panel white-text green lighten-2">
                                        {{-- {{ dd($errors->getBags()) }} --}}
                                        {{ $message }}
                                    </div>
                                    <hr>
                                @endif
                                <hr>
                                <table id="file_export" class="table table-striped display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Name</th>
                                            <th>IC Number</th>
                                            <th>Phone Number</th>
                                            <th>Email</th>
                                            <th>Last Sales</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dropships as $dropship)
                                            <tr>
                                                <td>{{ $dropship -> dropship_id }}</td>
                                                <td>{{ $dropship -> fullname }}</td>
                                                <td>{{ $dropship -> ic_number }}</td>
                                                <td>{{ $dropship -> phone_number }}</td>
                                                <td>{{ $dropship -> email }}</td>
                                                <td>{{ $dropship -> last_order_date }}</td>
                                                <td>
                                                    <button onclick="window.location.href='dropship-detail/{{ $dropship -> id }}'" class="waves-effect waves-light btn blue">View</button>
                                                    <button onclick="window.location.href='dropship-active/{{ $dropship -> id }}'" class="waves-effect waves-light btn green">Activate</button>
                                                </td>
                                            </tr>                                            
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

@push('datatables')
    <script src="{{asset('assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('#file_export').DataTable({
            buttons: [
                'copy', 'excel', 'pdf'
            ]
        });
    </script>
@endpush