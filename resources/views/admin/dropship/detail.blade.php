@extends('layouts.admin')

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <h5 class="card-title">Dropship Details</h5>
                                @if ($message = Session::get('success'))
                                    <hr>
                                    <div class="card-panel white-text green lighten-2">
                                        {{-- {{ dd($errors->getBags()) }} --}}
                                        {{ $message }}
                                    </div>
                                    <hr>
                                @endif
                                <div class="row">
                                    <div class="input-field col s12 m2 l2">
                                        <input id="id" type="text" value="{{ $dropship -> dropship_id }}" readonly>
                                        <label for="id">ID</label>
                                    </div>
                                    <div class="input-field col s12 m5 l5">
                                        <input id="fullname" type="text" value="{{ $dropship -> fullname }}" readonly>
                                        <label for="fullname">Full Name</label>
                                    </div>
                                    <div class="input-field col s12 m5 l5">
                                        <input id="ic_number" type="text" value="{{ $dropship -> ic_number }}" readonly>
                                        <label for="ic_number">Identification Number</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 m6 l6">
                                        <input id="email" type="email" value="{{ $dropship -> email }}" readonly>
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <input id="phone_number" type="text" value="{{ $dropship -> phone_number }}" readonly>
                                        <label for="phone_number">Phone Number</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="address" class="materialize-textarea" length="120" readonly>{{ $dropship -> address }}</textarea>
                                        <label for="address">Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button class="btn red waves-effect waves-light right" onclick="window.location.href='/dropship'">Cancel</button>
                                        <button class="btn blue waves-effect waves-light right" onclick="window.location.href='/dropship-update/{{ $dropship -> id }}'">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection