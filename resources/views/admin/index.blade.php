@extends('layouts.admin')

@push('customCSS')
    <link href="{{asset('assets/extra-libs/prism/prism.css')}}" rel="stylesheet">
@endpush

@section('content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <img style="bottom: 20px;right: 15px;position: absolute;opacity: 0.1;" src="{{asset('assets/images/logo-dexandra.png')}}">
            <!-- ============================================================== -->
            <!-- Title and breadcrumb -->
            <!-- ============================================================== -->
            
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Summery -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col l3 m6 s12">
                        <div class="card info-gradient card-hover">
                            <div class="card-content">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2 class="white-text m-b-5">
                                            @if (is_null($countAgent))
                                                0
                                            @else
                                                {{ $countAgent -> countAgent }}
                                            @endif
                                        </h2>
                                        <h6 class="white-text op-5 light-blue-text">Active Agent</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col l3 m6 s12">
                        <div class="card info-gradient card-hover">
                            <div class="card-content">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2 class="white-text m-b-5">
                                            @if (is_null($countDropship))
                                                0
                                            @else
                                                {{ $countDropship -> countDropship }}
                                            @endif
                                        </h2>
                                        <h6 class="white-text op-5">Active Dropship</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     
                    
                    <div class="col l3 m6 s12">
                        <div class="card success-gradient card-hover">
                            <div class="card-content">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2 class="white-text m-b-5">
                                            @if (is_null($totalSale))
                                                RM 0
                                            @else
                                                RM {{ $totalSale }}
                                            @endif
                                        </h2>
                                        <h6 class="white-text op-5 text-darken-2">Total Sales This Month</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col l3 m6 s12">
                        <div class="card warning-gradient card-hover">
                            <div class="card-content">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <h2 class="white-text m-b-5">
                                            {{ $countSuspend }}
                                        </h2>
                                        <h6 class="white-text op-5">Total Suspend Users</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col s12 l8">
                        <div class="card">
                            <div class="card-content">
                                <h4 class="card-title">Sales Per Month</h4>
                                <div>
                                    <canvas id="chart2" height="150"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- Container fluid scss in scafholding.scss -->
            <!-- ============================================================== -->
            <footer class="center-align m-b-30">All Rights Reserved by Materialart. Designed and Developed.</footer>
        </div>
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->

@endsection

<?php

    $totalSale = '';
    $month = '';

    foreach($salePerMonth as $saleMonth){
        $totalSale .=$saleMonth -> sumPerMonth.","; 

        $monthNum  = $saleMonth -> perMonth;
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('F'); // March

        $month .="'".$monthName."',";
    }

    $totalSale = substr($totalSale, 0, -1);
    $month = substr($month, 0, -1);


?>

@push('customChartJS')
    <script src="{{asset('assets/extra-libs/prism/prism.js')}}"></script>
    <script src="{{asset('assets/libs/chart.js/dist/Chart.min.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>

    <script>

    $('#scroll1, #scroll2, #scroll3, #scroll4').perfectScrollbar();

    new Chart(document.getElementById("chart2"), {
        type: 'bar',
        data: {
            labels: [<?php echo $month; ?>],
            datasets: [{
                label: "Total Sales",
                backgroundColor: 
                [
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex(),
                    getRandomColorHex()
                ],
                data: [<?php echo $totalSale; ?>]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return 'RM ' + value;
                        }
                    }
                }]
            },     
            plugins: {
                datalabels: {
                    align: 'end',
                    anchor: 'end',
                    color: "#000000",
                    display: function(context) {
                        return context.dataset.data[context.dataIndex]>0;
                    }
                }
            },
            legend: { display: false },
            title: {
                display: true,
                text: 'Total Sales Per Month'
            }
        }
    });

    function getRandomColorHex() {
        var hex = "0123456789ABCDEF",
            color = "#";
        for (var i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * 16)];
        }

        return color;
    }

    </script>
@endpush