<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">
    <title>NSTwin Management System</title>
    <link href="{{asset('assets/dist/css/style.css')}}" rel="stylesheet">
    <!-- This page CSS -->
    <link href="{{asset('assets/dist/css/pages/authentication.css')}}" rel="stylesheet">
    <link href="{{asset('assets/libs/toastr/build/toastr.min.css')}}" rel="stylesheet">
    <!-- This page CSS -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{asset('assets/images/bg-auth.png')}}) no-repeat left center;">
            <div class="container">
                <div class="row">
                    <div class="col s12 l8 m6 demo-text">
                        <span class="db"><img width="35px" height="35px" src="{{asset('assets/images/logo-dexandra.png')}}" alt="logo" /></span>
                        <span class="db"><img width="500px" src="{{asset('assets/images/logo-text.png')}}" alt="logo" /></span>
                        <h1 class="font-light m-t-40">Welcome to the <span class="font-medium white-text">Nstwin Agency Management System</span></h1>
                    </div>
                </div>
                <div class="auth-box auth-sidebar">
                    <div id="loginform">
                        
                        <div class="p-l-10">
                            <h5 class="font-medium m-b-0 m-t-40">Sign In to Admin</h5>
                            <small>Just login to your account</small>
                        </div>
                        <div class="p-l-10">
                            @if (count($errors) > 0)
                                <hr>
                                <div class="card-panel white-text red lighten-2">
                                    {{-- {{ dd($errors->getBags()) }} --}}
                                    {{ $errors->getBag('default')->first() }}
                                </div>
                                <hr>
                            @endif
                        </div>
                        <!-- Form -->
                        <div class="row">
                            <form class="col s12" action="login" method="POST">
                                @csrf
                                <!-- email -->
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="email" type="text" name="stokis_id" class="validate" required>
                                        <label for="email">Stokist ID</label>
                                    </div>
                                </div>
                                <!-- pwd -->
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" type="password" name="password" class="validate" required>
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                                <!-- pwd -->
                                <div class="row m-t-40">
                                    <div class="col s12">
                                        <button class="btn-large w100 blue accent-4" type="submit">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/materialize.min.js')}}"></script>
    <script src="{{asset('assets/libs/toastr/build/toastr.min.js')}}"></script>
</body>

</html>