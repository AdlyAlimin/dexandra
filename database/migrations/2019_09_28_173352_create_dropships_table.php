<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('dropship_id', '20');
            $table->string('fullname', '100');
            $table->string('ic_number', '15');
            $table->string('phone_number', '15');
            $table->string('email', '150');
            $table->text('address');
            $table->string('status', '20');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dropships');
    }
}
