<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SuspendUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suspendUser:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine");

        $suspend_period = DB::table('settings')
                            ->first();

        $dayCounts = DB::table('agents')
                        ->select(DB::raw("*,DATEDIFF(CURDATE(), last_order_date) as dayCount"))
                        ->get();

        foreach ($dayCounts as $day) {
            if($day->dayCount > $suspend_period->suspend_period){
                \Log::info("Suspend Agent");
                DB::table('agents')
                    ->where('agent_id', $day->agent_id)
                    ->update([
                        'status' => 'Inactive'
                    ]);
            }
        }
        

        // if(empty($dayCounts)){
        //     $dropships = DB::table('dropships')
        //                     ->select(DB::raw("DATEDIFF(CURDATE(), last_order_date) as dayCount"))
        //                     ->get();

        //     foreach ($dropships as $dropship) {
        //         if($dropship->dayCount > 1){
        //             DB::table('dropships')
        //                 ->where('dropship_id', $dropship->dropship_id)
        //                 ->update([
        //                     'status' => 'Inactive'
        //                 ]);
        //         }
        //     }

           
        // }

        $this->info('Demo:Cron Cummand Run successfully!');

    }
}
