<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SuspendDropship extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suspendDropship:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine");

        $suspend_period = DB::table('settings')
                            ->first();

        $dayCounts = DB::table('dropships')
                        ->select(DB::raw("*,DATEDIFF(CURDATE(), last_order_date) as dayCount"))
                        ->get();

        foreach ($dayCounts as $day) {
            if($day->dayCount > $suspend_period->suspend_period){
                \Log::info("Suspend Dropship");
                DB::table('dropships')
                    ->where('dropship_id', $day->dropship_id)
                    ->update([
                        'status' => 'Inactive'
                    ]);
            }
        }
        
    }
}
