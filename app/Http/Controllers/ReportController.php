<?php

namespace App\Http\Controllers;

use App\Services\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $report;

    public function __construct(Report $report)
    {
        $this->report = $report;
    }

    public function dateBetween(Request $request){

        $agentOrders = $this->report->agentOrder();
        $dropshipOrders = $this->report->dropshipOrder();
        $reportDatas = $this->report->dateRangeReport($request->firstDate, $request->secondDate);

        return view('admin.order.index', 
                compact(
                    'agentOrders',
                    'dropshipOrders',
                    'reportDatas'
                ));

    }
}
