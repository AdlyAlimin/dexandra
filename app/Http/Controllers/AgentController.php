<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;
use Validator;

class AgentController extends Controller
{
    public function index(){

        try{

            $agents = DB::table('agents')
                        ->get();

            return view('admin.agent.index', compact('agents'));

        }catch(Exception $e){
            dd($e);
        }

    }

    public function form(){
        return view('admin.agent.add');
    }

    public function store(Request $request){

        try{

            $request->validate([
                'agent_id' => 'required',
                'fullname' => 'required',
                'ic_number' => 'required',
                'phone_number' => 'required',
                'email' => 'required',
                'address' => 'required',
            ]);

            DB::table('agents')
                ->insert([
                    'agent_id' => $request -> agent_id,
                    'fullname' => $request -> fullname,
                    'ic_number' => $request -> ic_number,
                    'phone_number' => $request -> phone_number,
                    'email' => $request -> email,
                    'address' => $request -> address,
                    'status' => 'Active',
                    'last_order_date' => date('Y-m-d')
                ]);

            return redirect('/agent')->with('success', 'Agent Inserted Successful');

        }catch(Exception $e){
            return redirect()->back()->withError("Internal Server Error");
        }

    }

    public function detail($id){

        try{

            $agent = DB::table('agents')
                        ->where('id', $id)
                        ->first();

            return view('admin.agent.detail', compact('agent'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Agent not exist');
        }
    }

    public function view($id){

        try{

            $agent = DB::table('agents')
                        ->where('id', $id)
                        ->first();

            return view('admin.agent.update', compact('agent'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Agent not exist');
        }

    }

    public function update(Request $request, $id){

        try{
            DB::table('agents')
                ->where('id', $id)
                ->update([
                    'agent_id' => $request -> agent_id,
                    'fullname' => $request -> fullname,
                    'ic_number' => $request -> ic_number,
                    'phone_number' => $request -> phone_number,
                    'email' => $request -> email,
                    'address' => $request -> address
                ]);
            
            return redirect('/agent-detail/'.+$id)->with('success', 'Agent Updated Successful');

        }catch(Exception $e){
            return redirect()->back()->withErrors('Agent Update Error');
        }

    }

    public function delete($id){
        try{
            DB::table('agents')
                ->where('id', $id)
                ->delete();

            return redirect('/agent')->with('success', 'Agent Deleted');
        }catch(Exception $e){
            return redirect()->back()->withErrors('Agent Delete Error');
        }
    }

    public function suspendList(){
        try{

            $agents = DB::table('agents')
                        ->where('status', 'Inactive')
                        ->get();

            return view('admin.agent.suspend', compact('agents'));

        }catch(Exception $e){
            dd($e);
        }
    }

    public function activeAgent($id){
         try{

            DB::table('agents')
                ->where('id', $id)
                ->update([
                    'status' => 'Active'
                ]);

            return redirect('/agent')->with('success', 'Agent Status Activated');

         }catch(Exception $e){
             dd($e);
         }
    }
    
}
