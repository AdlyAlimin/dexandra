<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;

class DropshipController extends Controller
{
    public function index(){

        try{

            $dropships = DB::table('dropships')
                        ->get();

            return view('admin.dropship.index', compact('dropships'));

        }catch(Exception $e){
            dd($e);
        }

    }

    public function form(){
        return view('admin.dropship.add');
    }

    public function store(Request $request){

        try{

            $request->validate([
                'dropship_id' => 'required',
                'fullname' => 'required',
                'ic_number' => 'required',
                'phone_number' => 'required',
                'email' => 'required',
                'address' => 'required',
            ]);

            DB::table('dropships')
                ->insert([
                    'dropship_id' => $request -> dropship_id,
                    'fullname' => $request -> fullname,
                    'ic_number' => $request -> ic_number,
                    'phone_number' => $request -> phone_number,
                    'email' => $request -> email,
                    'address' => $request -> address,
                    'status' => 'Active',
                    'last_order_date' => date('Y-m-d')
                ]);

            return redirect('/dropship')->with('success', 'Dropship Inserted');

        }catch(Exception $e){
            return redirect()->back()->withError("Internal Server Error");
        }

    }

    public function detail($id){

        try{

            $dropship = DB::table('dropships')
                        ->where('id', $id)
                        ->first();

            return view('admin.dropship.detail', compact('dropship'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Dropship not exist');
        }
    }

    public function view($id){

        try{

            $dropship = DB::table('dropships')
                        ->where('id', $id)
                        ->first();

            return view('admin.dropship.update', compact('dropship'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Dropship not exist');
        }

    }

    public function update(Request $request, $id){

        try{
            DB::table('dropships')
                ->where('id', $id)
                ->update([
                    'dropship_id' => $request -> dropship_id,
                    'fullname' => $request -> fullname,
                    'ic_number' => $request -> ic_number,
                    'phone_number' => $request -> phone_number,
                    'email' => $request -> email,
                    'address' => $request -> address
                ]);
            
            return redirect('/dropship-detail/'.+$id)->with('success', 'Dropship Updated Successful');

        }catch(Exception $e){
            return redirect()->back()->withErrors('Dropship Update Error');
        }

    }

    public function delete($id){
        try{
            DB::table('dropships')
                ->where('id', $id)
                ->delete();

            return redirect('/dropship')->with('success', 'Dropship Deleted');
        }catch(Exception $e){
            return redirect()->back()->withErrors('Dropship Delete Error');
        }
    }

    public function suspendList(){
        try{

            $dropships = DB::table('dropships')
                            ->where('status', 'Inactive')
                            ->get();

            return view('admin.dropship.suspend', compact('dropships'));

        }catch(Exception $e){
            dd($e);
        }
    }

    public function activeDropship($id){
        try{
            DB::table('dropships')
                ->where('id', $id)
                ->update([
                    'status' => 'Active'
                ]);

            return redirect('/dropship')->with('success', 'Dropship Status Activated');
            
        }catch(Exception $e){
            dd($e);
        }
    }
}
