<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Validator;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function view(){

        try{
            $agentOrders = DB::table('orders')
                                ->where('cust_id', 'like', 'A%')
                                ->get();

            $dropshipOrders = DB::table('orders')
                                ->where('cust_id', 'like', 'D%')
                                ->get();

            return view('admin.order.index', compact('agentOrders', 'dropshipOrders'));
        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }
    }

    public function form(){
        
        $dropships = DB::table('dropships')
                        ->get();

        $agents = DB::table('agents')
                    ->get();

        return view('admin.order.view', compact('dropships', 'agents'));

    }

    public function agentDetail($id){
        
        try{

            $agent = DB::table('agents')
                        ->where('id', $id)
                        ->first();

            return view('admin.order.agent', compact('agent'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }
    }

    public function dropshipDetail($id){

        try{

            $dropship = DB::table('dropships')
                            ->where('id', $id)
                            ->first();

            return view('admin.order.dropship', compact('dropship'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }
    }

    public function detail($id, $cust_id){

        try{

            $cust = DB::table('orders')
                        ->where('orders.id', $id)
                        ->join('agents', 'agent_id', '=', 'orders.cust_id')
                        ->first();

            if(empty($cust)){
                $cust = DB::table('orders')
                            ->where('orders.id', $id)
                            ->join('dropships', 'dropship_id', '=', 'orders.cust_id')
                            ->first();
            }

            return view('admin.order.detail', compact('cust'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }

    }

    public function save(Request $request){

        try{

            DB::table('orders')
                ->insert([
                    'cust_id' => $request -> cust_id,
                    'sales' => $request -> sales,
                    'notes' => $request -> notes,
                    'sale_date' => $request -> sale_date
                ]);

            $cust = DB::table('agents')
                        ->where('agent_id', $request->cust_id)
                        ->update([
                            'last_order_date' => date('Y-m-d')
                        ]);

            if(is_null($cust)){
                DB::table('dropships')
                    ->where('agent_id', $request->cust_id)
                    ->update([
                        'last_order_date' => date('Y-m-d')
                    ]);
            }

            return redirect('/sales')->with('success', 'Sales Saved');
        }catch(Exception $e){
            return redirect()->back()->withErrors('Sales Has been Failed');
        }

    }

    public function deleteSale($id){

        try{
            DB::table('orders')
                ->where('id', $id)
                ->delete();
            
            return redirect()->back()->with('success', 'Sale Delete');
        }catch(Exception $e){
            dd($e);
        }
    }

}