<?php

namespace App\Http\Controllers;

use App\Services\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    protected $dashboard;

    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function index(){

        $totalSale = $this->dashboard->saleThisMonth();
        $salePerMonth = $this->dashboard->salePerMonth();

        $countAgent = 0;
        $countDropship = 0;
        $countMonthOrder = 0;
        $countSuspend = 0;

        $countAgent = DB::table('agents')
                        ->select(DB::raw("COUNT(*) as countAgent"))
                        ->where('status', 'Active')
                        ->first();

        $countDropship = DB::table('dropships')
                            ->select(DB::raw("COUNT(*) as countDropship"))
                            ->where('status', 'Active')
                            ->first();

        $countMonthOrder = DB::table('orders')
                            ->select(DB::raw('COUNT(*) as countMonthOrder'))
                            ->first();

        $countAgentSuspend = DB::table('agents')
                                ->select(DB::raw("COUNT(*) as countAgentSuspend"))
                                ->where('status', 'Inactive')
                                ->first();

        $countDropshipSuspend = DB::table('dropships')
                                    ->select(DB::raw("COUNT(*) as countDropshipSuspend"))
                                    ->where('status', 'Inactive')
                                    ->first();

        $countSuspend = $countAgentSuspend -> countAgentSuspend + $countDropshipSuspend -> countDropshipSuspend;

        return view('admin.index', 
                compact(
                    'countAgent', 
                    'countDropship', 
                    'countMonthOrder', 
                    'countSuspend',
                    'totalSale',
                    'salePerMonth'
                ));
    }

}
