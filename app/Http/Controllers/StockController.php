<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function index(){

        try{
            $stocks = DB::table('stocks')
                        ->get();

            return view('admin.stock.index', compact('stocks'));

        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }

    }

    public function form(){
        return view('admin.stock.add');
    }

    public function save(Request $request){

        try {
            //code...
            $stock = DB::table('stocks')
                        ->orderBy('quantity_date', 'desc')
                        ->first();

            if(is_null($stock)){
                $quantity = 0;
            }

            $quantity = $stock -> current_quantity;

            $new_quantity = $quantity + $request -> quantity;

            DB::table('stocks')
                ->insert([
                    'current_quantity' => $new_quantity,
                    'add_quantity' => $request -> quantity,
                    'minus_quantity' => 0,
                    'quantity_date' => date('Y-m-d')
                ]);
            

            redirect('/stock')->with('success', 'Quantity Added');
            
        } catch (\Exception $e) {
            //throw $th;
            return redirect()->back()->withErrors('Error To Update Stock Quantity');
        }
    }
}
