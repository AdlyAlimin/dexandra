<?php

namespace App\Http\Controllers;

use App\Services\Setting;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    protected $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }
    
    public function index(){

        $suspendPeriod = $this->setting->suspendPeriod();

        return view('admin.setting.suspend',
                compact(
                    'suspendPeriod'
                ));

    }

    public function periodForm($id){

        $setting = $this->setting->periodForm($id);

        return view('admin.setting.form.suspend', compact('setting'));

    }

    public function periodUpdate(Request $request, $id){

        try{
            
            DB::table('settings')
                ->where('id', $id)
                ->update([
                    'suspend_period' => $request->suspend_period
                ]);

            return redirect('/setting')->with('success', 'Suspend Period Changes');

        }catch(Exception $e){
            return redirect()->back()->withErrors('error', 'Data Not Found');
        }
    }
}
