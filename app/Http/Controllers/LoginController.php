<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    public function index(){
        return view('login.index');
    }

    public function check_login(Request $request){

        $check_login = User::where('stokis_id', $request -> stokis_id)
                            ->first();

        if(!$check_login instanceof User) return redirect()->back()->withErrors('Stokist Number not Match');

        if(!Hash::check($request -> password, $check_login -> password)) return redirect()->back()->withErrors('Password Not Match');

        $request->session()->put('stokis_id', $request -> stokis_id);

        return redirect('/home');
    }

    public function logout(){
        Session::flush();
        return redirect('/');
    }
}
