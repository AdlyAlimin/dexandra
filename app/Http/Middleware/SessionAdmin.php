<?php

namespace App\Http\Middleware;

use Closure;

class SessionAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(!$request->session()->get('stokis_id')=='Admin'){
        //     return redirect('logout');
        // }

        if(is_null($request->session()->get('stokis_id'))){
            return redirect('/logout');
        }
        
        return $next($request);
    }
}
