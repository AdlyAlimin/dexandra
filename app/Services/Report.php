<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Report{

  public function todayReport(){

    $todayReport = DB::table('orders')
                      ->where('sale_date', date('Y-m-d'))
                      ->get();

    return $todayReport;

  }

  public function dateRangeReport($firstDate, $secondDate){

    $dateRangeReport = DB::table('orders')
                          ->whereBetween('sale_date', [$firstDate, $secondDate])
                          ->get();

    return $dateRangeReport;

  }

  public function agentOrder(){

    $agentOrders = DB::table('orders')
                      ->where('cust_id', 'like', 'A%')
                      ->get();

    return $agentOrders;

  }

  public function dropshipOrder(){

    $dropshipOrders = DB::table('orders')
                        ->where('cust_id', 'like', 'D%')
                        ->get();

    return $dropshipOrders;
  }

}