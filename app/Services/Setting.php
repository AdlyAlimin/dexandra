<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Setting{

  public function suspendPeriod(){

    $suspendPeriod = DB::table('settings')
                        ->first();

    return $suspendPeriod;

  }

  public function periodForm($id){

    $setting = DB::table('settings')
                  ->where('id', $id)
                  ->first();

    return $setting;

  }

}