<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class Dashboard{

  public function saleThisMonth(){

    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $last_date = date('Y-m-d',strtotime('last day of this month'));

    $totalSale = DB::table('orders')
                    ->whereBetween('sale_date', [$first_date, $last_date])
                    ->sum('sales');

    if(is_null($totalSale)){
      $totalSale = 0;
    }

    return $totalSale;

  }

  public function salePerMonth(){

    $salePerMonth = DB::table('orders')
                      ->select(DB::raw("sum(sales) as sumPerMonth, month(sale_date) as perMonth"))
                      ->groupBy('perMonth')
                      ->get();

    return $salePerMonth;
  }

}