<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function view(){
        
        try{
            $orders = DB::table('orders')
                        ->get();

            return view('admin.order.index', compact('orders'));
        }catch(Exception $e){
            return redirect()->back()->withErrors('Internal Server Error');
        }
    }
}
