<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//basic system route
Route::get('/', 'LoginController@index');
Route::post('/login', 'LoginController@check_login');
Route::get('/logout', 'LoginController@logout');

Route::group(['middleware' => 'sessionadmin'], function(){
  Route::get('/home', 'DashboardController@index');

  //doute for agent
  Route::get('/agent', 'AgentController@index');
  Route::get('/agent-add', 'AgentController@form');
  Route::post('/agent-save', 'AgentController@store');
  Route::get('/agent-detail/{id}', 'AgentController@detail');
  Route::get('/agent-update/{id}', 'AgentController@view');
  Route::patch('/agent-update/{id}', 'AgentController@update');
  Route::get('/agent-delete/{id}', 'AgentController@delete');
  Route::get('/agent-suspend', 'AgentController@suspendList');
  Route::get('/agent-active/{id}', 'AgentController@activeAgent');
    
  //route for dropship
  Route::get('/dropship', 'DropshipController@index');
  Route::get('/dropship-add', 'DropshipController@form');
  Route::post('/dropship-save', 'DropshipController@store');
  Route::get('/dropship-detail/{id}', 'DropshipController@detail');
  Route::get('/dropship-update/{id}', 'DropshipController@view');
  Route::patch('/dropship-update/{id}', 'DropshipController@update');
  Route::get('/dropship-delete/{id}', 'DropshipController@delete');
  Route::get('/dropship-suspend', 'DropshipController@suspendList');
  Route::get('/dropship-active/{id}', 'DropshipController@activeDropship');
  
  //route for sales
  Route::get('/sales', 'OrderController@view');
  Route::get('/sales-new', 'OrderController@form');
  Route::post('/sales-save', 'OrderController@save');
  Route::get('/sales-detail/{id}/{cust_id}', 'OrderController@detail');
  Route::get('/sales-agentdetail/{id}', 'OrderController@agentDetail');
  Route::get('/sales-dropshipdetail/{id}', 'OrderController@dropshipDetail');
  Route::get('sales-delete/{id}', 'OrderController@deleteSale');
  
  //route for stock
  Route::get('/stock', 'StockController@index');
  Route::get('/stock-add', 'StockController@form');
  Route::post('/stock-save', 'StockController@save');

  //route for report
  Route::post('/report-sale', 'ReportController@dateBetween');

  //route for setting
  Route::get('/setting', 'SettingController@index');
  Route::get('/setting-period/{id}', 'SettingController@periodForm');
  Route::patch('/setting-period-save/{id}', 'SettingController@periodUpdate');
});